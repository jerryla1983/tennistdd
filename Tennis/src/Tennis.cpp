#include "Tennis.h"

void CTennisScore::PlayerAGetPoint( int nPoint )
{
    nPlayerAPoint = GetPoint( nPoint );
}

void CTennisScore::PlayerBGetPoint( int nPoint )
{
    nPlayerBPoint = GetPoint( nPoint );
}

std::string CTennisScore::Game()
{
    std::string strSore;

    if ( GameDeuce( strSore ) )
    {
        return strSore;
    }

    if ( GameCheck( strSore ) )
    {
        return strSore;
    }

    if ( IsMatchPoint() )
    {
        strSore = PointToScore( nPlayerAPoint );
        return strSore + "_All";
    }

    std::string strAPoint = PointToScore( nPlayerAPoint );
    std::string strBPoint = PointToScore( nPlayerBPoint );
    strSore = strAPoint + "_" + strBPoint;
    
    return strSore;

}

int CTennisScore::GetPoint( int nPoint )
{
    if ( nPoint > 0 )
    {
        return nPoint;
    }
    else
    {
        return -1;
    }
}

bool CTennisScore::GameCheck( std::string& strSore )
{
    if ( nPlayerAPoint >= 3 && nPlayerBPoint >= 3 )
    {
        int nDeuce = std::abs( nPlayerAPoint - nPlayerBPoint );
        if ( nDeuce >= 2 )
        {
            strSore = nPlayerAPoint > nPlayerBPoint ? "PlayerA Win" : "PlayerB Win";
            return true;
        }
    }
    else if ( nPlayerAPoint == 3 )
    {
        strSore = "PlayerA Win";
        return true;
    }
    else if ( nPlayerBPoint == 3 )
    {
        strSore = "PlayerB Win";
        return true;
    }
    return false;
}

bool CTennisScore::GameDeuce( std::string& strSore )
{
    if ( nPlayerAPoint >= 3 && nPlayerBPoint >= 3 )
    {
        int nDeuce = std::abs( nPlayerAPoint - nPlayerBPoint );

        if ( nDeuce == 0 )
        {
            strSore = "Deuce";
            return true;
        }
        else if ( nDeuce == 1 )
        {
            strSore = nPlayerAPoint > nPlayerBPoint ? "PlayerA AD" : "PlayerB AD";
            return true;
        }
    }
    return false;
}

std::string CTennisScore::PointToScore( int nPoint )
{
    if ( nPoint == 0 )
    {
        return "Love";
    }
    else if ( nPoint == 1 )
    {
        return "Fifteen";
    }
    else if ( nPoint == 2 )
    {
        return "Thirty";
    }
    else if ( nPoint == 3 )
    {
        return "Forty";
    }
    else
    {
        return "";
    }
}

bool CTennisScore::IsMatchPoint()
{
    if ( nPlayerAPoint == nPlayerBPoint )
    {
        return true;
    }
    else
    {
        return false;
    }
}