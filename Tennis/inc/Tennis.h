#pragma once

#include <string>

class CTennisScore
{
public:
    CTennisScore() = default;
    ~CTennisScore() = default;

public:
    void PlayerAGetPoint( int nPoint );
    void PlayerBGetPoint( int nPoint );
    int GetPlayerAPoint() { return nPlayerAPoint; }
    int GetPlayerBPoint() { return nPlayerBPoint; }

    std::string Game( );

protected:
    int GetPoint( int nPoint );
    bool GameCheck( std::string& strSore );
    bool GameDeuce( std::string& strSore );
    std::string PointToScore( int nPoint );
    bool IsMatchPoint();

private:
    int nPlayerAPoint = 0;
    int nPlayerBPoint = 0;

};
