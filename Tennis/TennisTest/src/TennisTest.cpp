// AccountingTest.cpp : Defines the entry point for the console application.
//

#include <gtest/gtest.h>
#include "Tennis.h"

using namespace testing;

class CTennisTest : public Test
{
protected:
    virtual void SetUp()
    {

    }

    CTennisScore kAccount;
};

TEST_F( CTennisTest, Tennis_A_Point_Error )
{
    kAccount.PlayerAGetPoint( 0 );
    EXPECT_EQ( -1, kAccount.GetPlayerAPoint() );

    //kAccount.PlayerAGetPoint( 4 );
    //EXPECT_EQ( -1, kAccount.GetPlayerAPoint() );
}

TEST_F( CTennisTest, Tennis_B_Point_Error )
{
    kAccount.PlayerBGetPoint( 0 );
    EXPECT_EQ( -1, kAccount.GetPlayerBPoint() );

    //kAccount.PlayerAGetPoint( 4 );
    //EXPECT_EQ( -1, kAccount.GetPlayerAPoint() );
}

TEST_F( CTennisTest, Tennis_Love_All )
{
    std::string str = kAccount.Game();
    EXPECT_EQ( "Love_All", str );
}

TEST_F( CTennisTest, Tennis_Fifteen_Love )
{
    kAccount.PlayerAGetPoint( 1 );
    std::string str = kAccount.Game();
    EXPECT_EQ( "Fifteen_Love", str );
}

TEST_F( CTennisTest, Tennis_Thirty_Love )
{
    kAccount.PlayerAGetPoint( 2 );
    std::string str = kAccount.Game();
    EXPECT_EQ( "Thirty_Love", str );
}

TEST_F( CTennisTest, Tennis_Forty_Love )
{
    kAccount.PlayerAGetPoint( 3 );
    std::string str = kAccount.Game();
    EXPECT_EQ( "PlayerA Win", str );
}

TEST_F( CTennisTest, Tennis_Love_Forty )
{
    kAccount.PlayerBGetPoint( 3 );
    std::string str = kAccount.Game();
    EXPECT_EQ( "PlayerB Win", str );
}

TEST_F( CTennisTest, Tennis_Love_Thirty )
{
    kAccount.PlayerBGetPoint( 2 );
    std::string str = kAccount.Game();
    EXPECT_EQ( "Love_Thirty", str );
}

TEST_F( CTennisTest, Tennis_Love_Fifteen )
{
    kAccount.PlayerBGetPoint( 1 );
    std::string str = kAccount.Game();
    EXPECT_EQ( "Love_Fifteen", str );
}

TEST_F( CTennisTest, Tennis_Fifteen_All )
{
    kAccount.PlayerAGetPoint( 1 );
    kAccount.PlayerBGetPoint( 1 );
    std::string str = kAccount.Game();
    EXPECT_EQ( "Fifteen_All", str );
}

TEST_F( CTennisTest, Tennis_Thirty_All )
{
    kAccount.PlayerAGetPoint( 2 );
    kAccount.PlayerBGetPoint( 2 );
    std::string str = kAccount.Game();
    EXPECT_EQ( "Thirty_All", str );
}

TEST_F( CTennisTest, Tennis_Forty_All )
{
    kAccount.PlayerAGetPoint( 3 );
    kAccount.PlayerBGetPoint( 3 );
    std::string str = kAccount.Game();
    EXPECT_EQ( "Deuce", str );
}

TEST_F( CTennisTest, Tennis_A_AD )
{
    kAccount.PlayerAGetPoint( 4 );
    kAccount.PlayerBGetPoint( 3 );
    std::string str = kAccount.Game();
    EXPECT_EQ( "PlayerA AD", str );
}

TEST_F( CTennisTest, Tennis_A_AD_GetPoint )
{
    kAccount.PlayerAGetPoint( 5 );
    kAccount.PlayerBGetPoint( 3 );
    std::string str = kAccount.Game();
    EXPECT_EQ( "PlayerA Win", str );
}

TEST_F( CTennisTest, Tennis_Duece_Again )
{
    kAccount.PlayerAGetPoint( 4 );
    kAccount.PlayerBGetPoint( 4 );
    std::string str = kAccount.Game();
    EXPECT_EQ( "Deuce", str );
}

TEST_F( CTennisTest, Tennis_B_AD )
{
    kAccount.PlayerAGetPoint( 4 );
    kAccount.PlayerBGetPoint( 5 );
    std::string str = kAccount.Game();
    EXPECT_EQ( "PlayerB AD", str );
}

TEST_F( CTennisTest, Tennis_B_AD_GetPoint )
{
    kAccount.PlayerAGetPoint( 4 );
    kAccount.PlayerBGetPoint( 6 );
    std::string str = kAccount.Game();
    EXPECT_EQ( "PlayerB Win", str );
}

TEST_F( CTennisTest, Tennis_Duece_ThreeTime )
{
    kAccount.PlayerAGetPoint( 5 );
    kAccount.PlayerBGetPoint( 5 );
    std::string str = kAccount.Game();
    EXPECT_EQ( "Deuce", str );
}

TEST_F( CTennisTest, Tennis_A_AD2 )
{
    kAccount.PlayerAGetPoint( 6 );
    kAccount.PlayerBGetPoint( 5 );
    std::string str = kAccount.Game();
    EXPECT_EQ( "PlayerA AD", str );
}

TEST_F( CTennisTest, Tennis_A_AD2_GetPoint )
{
    kAccount.PlayerAGetPoint( 7 );
    kAccount.PlayerBGetPoint( 5 );
    std::string str = kAccount.Game();
    EXPECT_EQ( "PlayerA Win", str );
}
